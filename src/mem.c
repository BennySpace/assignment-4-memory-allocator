#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(const void* addr, size_t query) {
    // Calculate the actual size required for the region including headers
    size_t region_size = region_actual_size(offsetof(struct block_header, contents) + query);
    
    // Attempt to map pages at the specified address with fixed mapping
    void* region_address = map_pages(addr, region_size, MAP_FIXED);

    // If fixed mapping fails, attempt to map pages without fixing the mapping
    if (region_address == MAP_FAILED) {
        region_address = map_pages(addr, region_size, 0);
    }

    // If mapping still fails, return a region with zero size
    if (region_address == MAP_FAILED) {
        return (struct region){0};
    } 

    // Initialize the block at the allocated region
    block_init(region_address, (block_size) {.bytes = region_size}, NULL);

    // Create and populate a struct region representing the allocated region
    struct region result;
    result.addr = region_address;
    result.size = region_size;
    result.extends = (region_address == addr);

    return result;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
    struct block_header* curr_header = HEAP_START;

    // Iterate through the linked list of block headers
    while (curr_header != NULL) {
        size_t curr_reg_size = 0;
        void* curr_reg_start = curr_header;

        // Calculate the size of the memory region represented by the current block header
        while (curr_header->next == block_after(curr_header)) {
            curr_reg_size += size_from_capacity(curr_header->capacity).bytes;
            curr_header = curr_header->next;
        }

        curr_reg_size += size_from_capacity(curr_header->capacity).bytes;
        curr_header = curr_header->next;

        // Unmap the memory region
        if (munmap(curr_reg_start, curr_reg_size) == -1) {
            break; // Exit loop if unmapping fails
        }
    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if (block_splittable(block, query)) {
        query = size_max(query, BLOCK_MIN_CAPACITY);
        void* new_block = block->contents + query;
        block_size size = (block_size) {.bytes = (block->capacity.bytes - query)};
        block_init(new_block, size, block->next);
        block->next = new_block;
        block->capacity.bytes = query;

        return true;
    }

    return false;
}

/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    if (block->next != NULL && mergeable(block, block->next)) {
        block->capacity = (block_capacity){.bytes = block->capacity.bytes + size_from_capacity(block->next->capacity).bytes};
        block->next = block->next->next;

        return true;
    }

    return false;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

static struct block_search_result find_good_or_last(struct block_header* restrict block, size_t sz)    {
    struct block_header* last_block;

    while (block) {
        while (try_merge_with_next(block));

        if (block->is_free && block_is_big_enough(sz, block)) {
            return (struct block_search_result) {
                    .type = BSR_FOUND_GOOD_BLOCK,
                    .block = block
            };
        }
        
        last_block = block;
        block = block->next;
    }
    
    return (struct block_search_result) {
            .type = BSR_REACHED_END_NOT_FOUND,
            .block = last_block
    };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header* block) {
    // Find a suitable block within the given block header
    struct block_search_result result = find_good_or_last(block, query);

    // If no suitable block is found, return the result
    if (result.type != BSR_FOUND_GOOD_BLOCK) {
        return result;
    }

    // Split the block if it's too big for the requested size
    split_if_too_big(result.block, query);

    // Mark the block as allocated
    result.block->is_free = false;

    return result;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    struct region new_region = alloc_region(block_after(last), query);

    if (region_is_invalid(&new_region)) {
        return NULL;
    }

    last->next = new_region.addr;

    if (new_region.extends && try_merge_with_next(last)) {
        return last;
    }

    return new_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc(size_t query, struct block_header* heap_start) {
    // Ensure that the requested size is at least BLOCK_MIN_CAPACITY
    query = (query > BLOCK_MIN_CAPACITY) ? query : BLOCK_MIN_CAPACITY;

    // Try to allocate memory within existing blocks
    struct block_search_result result = try_memalloc_existing(query, heap_start);
    
    // If no suitable block is found, grow the heap and try allocation again
    if (result.type == BSR_REACHED_END_NOT_FOUND) {
        struct block_header* new_block = grow_heap(result.block, query);
        result = try_memalloc_existing(query, new_block);
    }

    // Return the allocated block header if successful, otherwise NULL
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        return result.block;
    }

    return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
    if (!mem) return ;
    struct block_header* header = block_get_header( mem );
    header->is_free = true;

    // Try to merge the block with the next block if possible
    try_merge_with_next(header);
}
