#include "mem.h"
#include <assert.h>
#include <stdio.h>

#define LINE "###############"
#define SMALL_BLOCK_SIZE 100
#define MEDIUM_BLOCK_SIZE 500
#define LARGE_BLOCK_SIZE 100000
#define EXPANDED_BLOCK_SIZE 500000


void test_init(const char* test_name, void (*function)()) {
    heap_init(0);
    printf("%s\n", LINE);
    printf("%s\n", test_name);
    function();
    heap_term();
    printf("Test %s: Success.\n", test_name);
    printf("%s\n", LINE);
}


void assert_condition(bool condition, const char* message) {
    if (!condition) {
        printf("Assertion failed: %s\n", message);
        assert(condition);
    }
}


void first_test() {
    void* first_mem_block = _malloc(SMALL_BLOCK_SIZE);
    assert_condition(first_mem_block != NULL, "Memory allocation failed: Unable to allocate memory!");
    _free(first_mem_block);
}


void second_test() {
    void* first_mem_block = _malloc(SMALL_BLOCK_SIZE);
    void* second_mem_block = _malloc(MEDIUM_BLOCK_SIZE);
    _free(first_mem_block);
    _free(second_mem_block);
}


void third_test() {
    void* first_mem_block = _malloc(SMALL_BLOCK_SIZE);
    void* second_mem_block = _malloc(MEDIUM_BLOCK_SIZE);
    _free(first_mem_block);
    _free(second_mem_block);
}


void fourth_test() {
    void* first_mem_block = _malloc(LARGE_BLOCK_SIZE);
    assert_condition(first_mem_block != NULL, "Memory allocation failed: Unable to allocate memory.");
    _free(first_mem_block);
}


void fifth_test() {
    void* first_mem_block = _malloc(LARGE_BLOCK_SIZE);
    void* second_mem_block = _malloc(LARGE_BLOCK_SIZE);
    assert_condition(first_mem_block != NULL && second_mem_block != NULL, "Memory allocation failed.");

    _free(first_mem_block);

    void* mem3 = _malloc(EXPANDED_BLOCK_SIZE);
    assert_condition(mem3 != NULL, "Memory expansion with address limitation failed: Unable to allocate memory.");

    _free(second_mem_block);
    _free(mem3);
}


int main() {
    test_init("TEST #1: successful_memory_allocation", first_test);
    test_init("TEST #2: single_block_free", second_test);
    test_init("TEST #3: double_block_free", third_test);
    test_init("TEST #4: memory_expansion", fourth_test);
    test_init("TEST #5: memory_expansion_with_address_limitation", fifth_test);

    return 0;
}
